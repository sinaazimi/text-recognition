
import React, {useState} from 'react';
import {View , Text , StyleSheet , FlatList ,Image , Dimensions , TouchableOpacity, TextInput ,Alert , ScrollView} from 'react-native'
import NavigationBar from '../Components/NavigationBar'
import ControlBar from '../Components/ControlBar'
import Camera from "./Camera";
import { Icon } from 'react-native-elements'
import ResultHandle from './../Components/ResultHandle'
import ControBar from './../Components/ControlBar'
import Clipboard from '@react-native-community/clipboard';

const {width , height} = Dimensions.get('window');
const navigationBarHeight = 0.09 * height;

function copyToClipboard(text){
    Clipboard.setString(text);
    Alert.alert("Copied To Clipboard")
}

const ResultScreen = (props) => {
    const [pick,setPick] = useState(null)
    let recognizedText = ''
    let uri = ''
    let mode = ''
    let date = ''
    try{
      recognizedText =  props.navigation.state.params.Text
      uri = props.navigation.state.params.Uri
      mode = props.navigation.state.params.mode
      date = props.navigation.state.params.date

    }catch(err){
      console.error(err);
      recognizedText ='';
    }

    return (
    <>
    <View style={styles.page}>
        <View style={styles.resultContainer}>
          <TouchableOpacity onPress={()=>{props.navigation.navigate('ImageView',{src:uri})}} >
            <Image source = {{uri: uri,  
                        width: 200, 
                        height: 200}}
                  style={{marginLeft: (19*width/40) - 100 , marginTop : 20}}            
            />
        </TouchableOpacity>
            <ScrollView  style={styles.resultTextContainer} showsVerticalScrollIndicator={false} >
            <Text onPress={() => {copyToClipboard(recognizedText)}} style={styles.result}>{recognizedText}</Text>
            </ScrollView>
        </View> 
        <ResultHandle result={recognizedText} image={uri} mode={mode} date={date} navigation={props.navigation}/>
        <NavigationBar properties={props}/>
    </View>
    
    </>
    );
};

const styles = StyleSheet.create({
    page : {
      height : height , 
      width : width , 
      backgroundColor : '#F8F8F8' ,
      position : 'absolute' , 
      top : 0 ,
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor : '#F8F8F8' ,
      
    },

    libraryIcon : {
    },
    moreIcon : {
    
    },
    scanIcon :{
      marginLeft :width/4.11 , 
      marginRight :width/4.11 , 
      marginBottom : height/7.79

    },    
    resultContainer : {
        height : height/1.8 , 
        width : width - width/20 ,
        marginLeft : width/40 ,
        marginTop : width/15 ,  
        borderRadius :20 ,  
        elevation:3 ,
        backgroundColor : '#EEEFF1' ,
    }, 
    result:{
      fontSize : 18 , 
      textAlign : 'center'
    }, 
    resultTextContainer:{
      marginLeft : (19*width/40) - 250 , 
      width :500 ,
      marginTop : 10 , 
      
    }
});

export default ResultScreen