import React, {useState , useEffect} from 'react';
import { Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator , FlatList ,
TouchableOpacity  , Linking } from 'react-native';
import { ListItem , Card , Icon , Button} from 'react-native-elements'
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';

var RNFS = require('react-native-fs');
import NavigationBar from './../Components/NavigationBar'
import {sendEmail} from './../Components/SendEmail'

const {width , height} = Dimensions.get('window');
//780,411
const navigationBarHeight = 0.09 * height;

const list = [
    ['Tips',require('../Assets/lightbulb.png')] , 
    ['Feedback' , require('../Assets/writing.png') ],  
    ['About Me' , require('../Assets/linkedin.png')],
    ['Rate us' , require('../Assets/rating.png')],
    ['Clear data',require('../Assets/clean.png')] ,
    ['Theme (Comming Soon)',require('../Assets/theme.png')],
]
function hadndleOptions(item,props){
    switch(item){
        case 'Feedback' :{
            sendEmail(
                'sinaazm15@gmail.com',
                'Feedback on text recognition app',
                ''
            ).then(() => {
                console.log('Our email successful provided to device mail ');
            });
            break;
        }
        case 'About Me' : {
            const url = 'https://www.linkedin.com/in/sina-azimi-05762a174/'
            Linking.canOpenURL(url).then(supported => {
              if (supported) {
                Linking.openURL(url);
              } else {
                console.log("Don't know how to open URI: " + url);
              }
            });
            break;     
        }
        case 'Tips' : {
            console.log('tips')
            props.navigation.navigate('Tips')
            break;
        }
        case 'Clear data' : 
            Alert.alert(
                "Clear data",
                "All your saved recognitions will be lost. Are you sure ?",
                [
                {
                    text: "Cancel",
                    style: 'cancel'
                },
                { text: "OK", onPress: () => clearData() }
                ],
                { cancelable: true }
            );
            break;
    }
}
async function clearData(){
    let keys = ''
    try {
        keys = await AsyncStorage.getAllKeys()
        await AsyncStorage.multiRemove(keys)
    } catch(e) {
      console.log(e)
    }
}
export const Options = (props) => {
    const Item = ({title , src}) => (
        <View style={{flexDirection: 'column'}}>
            <TouchableOpacity onPress={() => {hadndleOptions(title,props)}}>
            <View style={styles.card}>
                <Text style={styles.text}>{title}</Text>
                <Image style={styles.icon} source={src}></Image>
            </View>
            </TouchableOpacity>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item title={item[0]} src={item[1]} />
        );
    return (
        <View style={styles.page}>
            <View style={styles.resultContainer}>
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={item => item}
                />
            </View>
            
            <NavigationBar properties={props}/>
        </View>

  );
}

const styles = StyleSheet.create({
    page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        marginTop:0 
        },
    resultContainer : {
        height : height - navigationBarHeight - 50 , 
        width : width,
        // flexDirection : 'row'
    },
    card : {
        width: width*2/3 , 
        backgroundColor : '#EEEFF1' , 
        height : 75 , 
        borderRadius : 15 , 
        marginLeft : width/6 ,
        marginTop : 40, 
        marginBottom : 5 ,
        alignItems:"center" , 
        // justifyContent : 'center' ,
        elevation:3 ,
        flexDirection : 'row'
    }, 
    text:{
        fontSize : 20 , 
        marginLeft : 20
    },
    icon:{
        height:40 , 
        width : 40 ,
        position : 'absolute' ,
        right : 10
    }
});

export default Options;
