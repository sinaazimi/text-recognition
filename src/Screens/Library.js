import React, {useState , useEffect} from 'react';
import { Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator , FlatList
} from 'react-native';
import { ListItem , Card , Icon , Button} from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage';

import NavigationBar from './../Components/NavigationBar'

const {width , height} = Dimensions.get('window');
//780,411
const navigationBarHeight = 0.09 * height;




const getData = async () => {
    let keys = []
    let values = []
    let results = []
    try {
        keys = await AsyncStorage.getAllKeys()
        values = await AsyncStorage.multiGet(keys)
        for(i=0;i<values.length;i++){
            results.push(JSON.parse(values[i][1]))
        }
        results = results.sort(function(a,b) {
            if(a.time<b.time){
               
                return +1
            }
            if(a.time > b.time){
                return -1
            }
            else{
                return 0
            }
        });

        return results
    } catch(e) {
      // error reading value
    }
  }
async function fetchData() {
    const values  = await getData()
    list =  values
}
fetchData()


export const Library = (props) => {
    const [list,setList] = useState('')
    const Item = ({ title , caption , path}) => (
        <View style={{flexDirection: 'row'}}>
            <View style={{width:width/2}}>
                <Card containerStyle={{
                    width:width/2.16 ,elevation:3 , marginLeft:width/68.5 ,marginBottom:10 ,
                    marginTop:10 , height:height/3.39 , borderRadius : 15
                     }}>
                    <Card.Title>{title}</Card.Title>
                    <Card.Divider/>    
                    <View style={{height : height/7.8}}>
                        <Text style={{marginBottom: 10}}>
                        {caption.slice(0,50)}
                        </Text>
                    </View>
                    <Button
                        buttonStyle={{borderRadius: 5, marginLeft: 0, marginRight: 0,}}
                        title='VIEW NOW'
                        type="outline"
                        titleStyle={{color:'#27BC62'}}
                        onPress={() => {
                            props.navigation.navigate(
                                'Result',{Text: caption , Uri: path , date:title ,mode:'old'})}}
                    />
                </Card>
            </View>
    
        </View>
    );
    useEffect(() => {
        async function fetchMyAPI() {
            const values  = await getData()
            // list =  values
            setList(values)
        }
        fetchMyAPI()
        },[])

    const renderItem = ({ item }) => (
        <Item title={item.time} caption={item.result} path={item.image} />
        );
    return (
        <View style={styles.page}>
            <View style={styles.resultContainer}>
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={item => item.image}
                    numColumns={2}
                />
            </View>
            
            <NavigationBar properties={props}/>
        </View>

  );
}

const styles = StyleSheet.create({
    page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        marginTop:0 
        },
    resultContainer : {
        height : height - navigationBarHeight - 50 , 
        width : width,
         
        flexDirection : 'row'
    },
    oneItemContainer : {
        width : width/2 ,
        justifyContent : 'center' , 
        alignItems : 'center' , 
        // backgroundColor : 'red' ,

    } ,
});

export default Library;
