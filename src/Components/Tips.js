import React, {useState , useEffect} from 'react';
import { Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator , FlatList ,
TouchableOpacity  , Linking} from 'react-native';
import { ListItem , Card , Icon , Button} from 'react-native-elements'
import ImagePicker from 'react-native-image-crop-picker';
var RNFS = require('react-native-fs');
import NavigationBar from './../Components/NavigationBar'
import {sendEmail} from './../Components/SendEmail'

const {width , height} = Dimensions.get('window');
//780,411
const navigationBarHeight = 0.09 * height;

const list = [
    ['Better accuracy','For the best accuracy try to cut your image precisely. Avoid unrealted objects in your image. Also try too zoom in, as much as possible for getting bette results.'] , 
    ['Storage' , 'Images you take or images picked from gallery will automatically save in application data folder. Try to delete the useless ones once in a while.'],  
    ['Recognition' ,'The recognition is done by tesseract and tried to be as accurate as possible.' ],
]

export const Tips = (props) => {
    const Item = ({title , caption}) => (
        <View style={{flexDirection: 'column'}}>
            <View style={styles.card}>
                <Text style={styles.text}>{title}</Text>
                <Text style={styles.caption}>{caption}</Text>
            </View>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item title={item[0]} caption={item[1]} />
        );
    return (
        <View style={styles.page}>
            <View style={styles.resultContainer}>
                <FlatList
                    data={list}
                    renderItem={renderItem}
                    keyExtractor={item => item}
                />
            </View>
            
            <NavigationBar properties={props}/>
        </View>

  );
}

const styles = StyleSheet.create({
    page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        marginTop:0 
        },
    resultContainer : {
        height : height - navigationBarHeight - 50 , 
        width : width,
        // flexDirection : 'row'
    },
    card : {
        width: width*3/4 , 
        backgroundColor : '#EEEFF1' , 
        height : 150 , 
        borderRadius : 15 , 
        marginLeft : width/8 ,
        marginTop : 40, 
        marginBottom : 5 ,
        // alignItems:"center" , 
        // justifyContent : 'center' ,
        elevation:3 ,
    }, 
    text:{
        fontSize : 20 , 
        marginLeft : 10 , 
        marginTop : 10
    },
    caption : {
        fontSize : 16 , 
        marginLeft : 10 , 
        marginTop : 10 , 
        maxWidth : width*3/4 , 
        marginRight : 5
    }
});

export default Tips;
