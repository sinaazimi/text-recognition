import React from "react";
import { Text, StyleSheet ,View , Button , Image, TouchableOpacity  , Dimensions} from "react-native";
import { Icon } from 'react-native-elements'


const {width , height} = Dimensions.get('window');

const controlBarHeight = 0.088 * height;


const ControlBar = (props) => {

  return(
    <View style={styles.controlBar}>
      
      <Icon
        name='camera-outline'
        type='ionicon' 
        size={35}
        onPress={() => {props.camera()}}
        iconStyle={styles.cameraIcon}
        raised={false}
      />
      
      <Icon
        name='image-outline'
        type='ionicon' 
        size={35}
        onPress={() => {props.gallery()}}
        iconStyle={styles.galleryIcon}
        raised={false}
        containerStyle={{marginLeft:70}}
      />
        
    </View>
  );
};

const styles = StyleSheet.create({
  controlBar :{
    backgroundColor : '#EEEFF1' , 
    width : width*3/5, 
    height : controlBarHeight, 
    alignItems : 'center',
    justifyContent :'center' ,
    flexDirection:'row' ,
    borderRadius :20 ,
    position : 'absolute' , 
    bottom : height/10  , 
    left : width/5,
    elevation:3 ,
  }, 
  cameraIcon : {
    
  },
  galleryIcon : {

  },

});

export default ControlBar;
