import React, {useState , useEffect} from 'react';
import { Alert ,StyleSheet, Text, View, Image , Dimensions, ActivityIndicator , FlatList ,
TouchableOpacity  , Linking} from 'react-native';
import { ListItem , Card , Icon , Button} from 'react-native-elements'
import ImagePicker from 'react-native-image-crop-picker';
var RNFS = require('react-native-fs');
import NavigationBar from './../Components/NavigationBar'
import {sendEmail} from './../Components/SendEmail'

const {width , height} = Dimensions.get('window');
//780,411
const navigationBarHeight = 0.09 * height;

export const ImageView = (props) => {
    let src = props.navigation.state.params.src
    return (
        <View style={styles.page}>
            <Image source={{uri:src}} style={styles.image}></Image>
            
            <NavigationBar properties={props}/>
        </View>

  );
}

const styles = StyleSheet.create({
    page : {
        height : height , 
        width : width , 
        backgroundColor : '#F8F8F8' ,
        marginTop:0 
        },
    resultContainer : {
        height : height - navigationBarHeight - 50 , 
        width : width,
    }, 
    image : { 
        marginLeft : 5.5 ,
        width : 400 , 
        height : 700 , 
        resizeMode : 'contain'
    }
});

export default ImageView;
