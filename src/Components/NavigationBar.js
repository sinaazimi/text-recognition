import React from "react";
import { Text, StyleSheet ,View , Button , Image, TouchableOpacity  , Dimensions} from "react-native";
import { Icon } from 'react-native-elements'

const {width , height} = Dimensions.get('window');

const navigationBarHeight = 0.16 * height;

const NavigationBar = (props) => {
  const barProps =  props.properties

  return(
      <View style={styles.navigationBar}>

        <Icon
            name='ellipsis-horizontal-outline'
            type='ionicon' 
            size={40}
            containerStyle={styles.moreIcon}
            onPress={() => {barProps.navigation.navigate('Options')}}
        />
        {/* <Icon
            name='barcode-outline'
            type='ionicon' 
            size={40}
            iconStyle={styles.scanIcon}
            containerStyle={styles.scanIconCont}
            color = '#27BC62'
            onPress={() => {barProps.navigation.navigate('Home')}}
            raised={true}
        /> */}
        <TouchableOpacity onPress={() => {barProps.navigation.navigate('Home')}}>
          <View style={styles.scanIconCont}>
            <Image style={styles.scanIcon} source={require('./../Assets/barcode-nb.png')}></Image>
          </View>
        </TouchableOpacity>
        <Icon
            name='grid-outline'
            type='ionicon' 
            size={40}
            containerStyle={styles.libraryIcon}
            onPress={() => {barProps.navigation.navigate('MyLibrary')}}
            
        />


      </View>
  );
};

const styles = StyleSheet.create({
  navigationBar :{
    backgroundColor : '#EEEFF1' , 
    width : width, 
    height : navigationBarHeight, 
    position : 'absolute' , 
    bottom : 0 ,
    alignItems : 'center' , 
    justifyContent : 'center' ,
    flexDirection : 'row' ,
    elevation:3 ,
  }, 
  libraryIcon : {
    marginLeft : width/4.8 ,
    marginBottom : 50
  },
  moreIcon : {
    marginRight : width/4.8 , 
    marginBottom : 50
  },
  scanIcon :{
    height:40 , 
    width:40 ,     
  }, 
  scanIconCont : {
    marginBottom : navigationBarHeight , 
    justifyContent : 'center' , 
    alignItems:'center' ,
    elevation :3 , 
    height:75 , 
    width:75 , 
    borderRadius : 37.5 ,
    backgroundColor:'white' , 
    
    
  }

});

export default NavigationBar;
