import React from "react";
import { Text, StyleSheet ,View , Button , Image, Alert ,TouchableOpacity , PermissionsAndroid  , Dimensions , Linking} from "react-native";
import { Icon , ListItem} from 'react-native-elements'
import AsyncStorage from '@react-native-async-storage/async-storage';
import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

const {width , height} = Dimensions.get('window');

const controlBarHeight = 0.088 * height;
const navigationBarHeight = 0.09 * height;

function handleGoogleSearch(result) {
  
  const url = 'https://www.google.com/search?q='.concat(result)
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log("Don't know how to open URI: " + url);
    }
  });
};

function handleGoogleTranslate(result) {

  const url = 'https://translate.google.com/?sl=auto&tl=fa&op=translate&text='.concat(result)
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      console.log("Don't know how to open URI: " + url);
    }
  });
};

const storeData = async (value) => {
  const datetime = getDate()
  let time = {time : datetime}
  value = {...time,...value};
  try {
    await AsyncStorage.setItem(datetime,  JSON.stringify(value))
  } catch (e) {
    console.log(e)
  }
}
const removeData = async (value) => {
  
  try {
    await AsyncStorage.removeItem(value.date)
    value.navigation.navigate('MyLibrary')
  } catch (e) {
    console.log(e)
  }
}
function getDate() {
    var currentdate = new Date(); 
    var datetime = "Time: " 
    let day = currentdate.getDate() + "/"
    if(day.length<3){day = '0'+day}
    let month =  (currentdate.getMonth()+1)  + "/" 
    if(month.length<3){month = '0'+month}
    let year =  currentdate.getFullYear() + " - "   
    if(year.length<3){year = '0'+year}
    let hour =  currentdate.getHours() + ":"  
    if(hour.length<3){hour = '0'+hour}
    let min =  currentdate.getMinutes() + ":" 
    if(min.length<3){min = '0' + min}
    let sec =  currentdate.getSeconds();
    if(sec<10){sec = '0' + sec}
    datetime = day+month+year+hour+min+sec
    return datetime
}
async function  createPDF(value) {
  const regex = /\\n|\\r\\n|\\n\\r|\\r/g;
  let text  = value.slice(0,-1) ;
  text = text.substring(1);
  text = text.replace(regex, '<br>');
    let options = {
      html:  '<p>' + text + '</p>'  ,
      fileName: text.slice(0,4),
      directory : 'TextRecognitionDocuments'
    };
    let file = await RNHTMLtoPDF.convert(options);
    Alert.alert('Pdf file saved in:',file.filePath);
  }
const ResultHandle = (props) => {

  return(
    <View style={styles.controlBar}>
    
      <TouchableOpacity
        onPress={() => handleGoogleTranslate(props.result) }>
            <Image 
                source={require('./../Assets/translate.png')}
                style={styles.translateIcon}
            />
        </TouchableOpacity>

        {props.mode == 'new' ? 
            <TouchableOpacity
            onPress={() => storeData(props) }>
                <Image 
                    source={require('./../Assets/bookmark.png')}
                    style={styles.saveIcon}
                />
            </TouchableOpacity>
        :
            <TouchableOpacity
            onPress={() => removeData(props) }>
                <Image 
                    source={require('./../Assets/delete.png')}
                    style={styles.saveIcon}
                />
            </TouchableOpacity>
        
        }

        <TouchableOpacity
          onPress={() => handleGoogleSearch(JSON.stringify(props.result))}>
            <Image 
                source={require('./../Assets/google.png')}
                style={styles.googleIcon}
            />
        </TouchableOpacity>
        
        <TouchableOpacity
          onPress={() => createPDF(JSON.stringify(props.result))}>
            <Image 
                source={require('./../Assets/pdf-file.png')}
                style={styles.pdfIcon}
            />
        </TouchableOpacity>
    </View>
  );
};
 
const styles = StyleSheet.create({
  controlBar :{
    backgroundColor : '#EEEFF1' , 
    width : width*3/5, 
    height : controlBarHeight, 
    alignItems : 'center',
    justifyContent :'center' ,
    flexDirection:'row' ,
    borderRadius :20 ,
    position : 'absolute' , 
    // bottom : height/5  , 
    bottom : navigationBarHeight + 50 + (height/10),
    left : width/5 , 
    elevation:3 ,
  }, 
  translateIcon : {
    marginRight : width/16.44  ,
    resizeMode : 'contain' , 
    width : 35 , 
    height : 35 , 
  },
  saveIcon : {
    resizeMode : 'contain' , 
    width : 35 , 
    height : 35 , 
    
  },
  googleIcon : {
    resizeMode : 'contain' , 
    width : 35 , 
    height : 35 , 
    marginLeft : width/16.44
  },
  pdfIcon : { 
    resizeMode : 'contain' , 
    width : 35 , 
    height : 35 , 
    marginLeft : width/16.44
  } 
}); 

export default ResultHandle;
